<div align="center">
<img src="./logo.png" width="300" >
<h1>Processo Seletivo 20.2</h1>
<h2>Exercícios JS - aula 2</h2>
</div>

### Questao 1 (Aula)
Imprima o nome e a quantidade de features de todos os deuses usando uma única linha de código.

### Questao 2 (Aula)
Imprima todos os deuses que possuem o papel de "Mid"

### Questao 3 
Organize a lista pelo panteão do deus.

### Questao 4
 Faça um código que retorne um novo array com o nome de cada deus e entre parênteses, a sua classe. Por exemplo, o array deverá ficar assim: ["Achilles (Warrior)", "Agni (Mage)", ...]

### Questao 5 
Faça um código que retorne um novo array apenas com o(s) deus(es) que seja da classe "Hunter", do panteão "Greek" e possua a role "Mid".
